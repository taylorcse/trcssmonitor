// -*-c++-*-

/*!
  \file plaeyr_painter.cpp
  \brief player painter class Source File.
  */

/*
 *Copyright:

 Copyright (C) The RoboCup Soccer Server Maintenance Group.
 Hidehisa Akiyama

 This code is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This code is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this code; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

 *EndCopyright:
 */

/////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <QtGui>

#include "zone_painter.h"

#include "disp_holder.h"
#include "options.h"
#include "circle_2d.h"
#include "vector_2d.h"

#include <rcsslogplayer/types.h>

#include <cstring>
#include <cstdio>
#include <cmath>

// Modified includes
#include <QPainterPath>
#include <fstream>
#include <iostream>
#include <string>

namespace {
    const double DEG2RAD = M_PI / 180.0;
}

/*-------------------------------------------------------------------*/
/*

*/
    inline
ZonePainter::Param::Param(  const rcss::rcg::PlayerT & player,
        const rcss::rcg::BallT & ball,
        const rcss::rcg::ServerParamT & sparam,
        const rcss::rcg::PlayerTypeT & ptype )
    : x_( Options::instance().screenX( player.x_ ) )
    , y_( Options::instance().screenY( player.y_ ) )
    , body_radius_( Options::instance().scale( ptype.player_size_ ) )
      , kick_radius_( Options::instance().scale( ptype.player_size_ + ptype.kickable_margin_ + sparam.ball_size_ ) )
      //, have_full_effort_( std::fabs( player.effort_ - ptype.effort_max_ ) < 1.0e-3 )
    , player_( player )
    , ball_( ball )
      , player_type_( ptype )
{
    if ( body_radius_ < 1 ) body_radius_ = 1;
    if ( kick_radius_ < 5 ) kick_radius_ = 5;

    draw_radius_ =  ( Options::instance().playerSize() >= 0.01
            ? Options::instance().scale( Options::instance().playerSize() )
            : kick_radius_ );
}

ZonePainter::Zone::Zone(double x1, double x2, double y1, double y2) :
    x1(x1), 
    x2(x2),
    y1(y1),
    y2(y2){}

/*-------------------------------------------------------------------*/
/*

*/
    ZonePainter::ZonePainter( const DispHolder & disp_holder )
: M_disp_holder( disp_holder )
{
    pen = QPen("red");
    //std::ifstream zone_file(Options::instance().zoneFile().c_str());
    //if (zone_file.is_open()) {
    //    std::string line;
    //    int num = 0;
    //    while (getline(zone_file, line)) {
    //        double x1,x2,y1,y2;
    //        if (strlen(line.c_str()) == 0) continue;
    //        sscanf(line.c_str(),"%lf,%lf,%lf,%lf",&x1,&x2,&y1,&y2);
    //        zones[num] = Zone(x1,x2,y1,y2);
    //        //printf("%lf\n",zones[num].y1);
    //        num++;
    //    }
    //    zone_file.close();
    //    std::cout << "Zones:" << std::endl;
    //    //for (int i=0; i < num; i++)
    //    //    printf("#%d - x1:%lf, y1:%lf, x2:%lf, y2:%lf\n", i, zones[i].x1, zones[i].y1, zones[i].x2, zones[i].y2);
    //} else {
    //    std::cout << "Zone file not found" << std::endl;
    //}
}

/*-------------------------------------------------------------------*/
/*

*/
ZonePainter::~ZonePainter()
{

}

/*-------------------------------------------------------------------*/
/*

*/
    void
ZonePainter::draw( QPainter & painter )
{
    if ( ! Options::instance().showPlayer() )
    {
        return;
    }

    DispConstPtr disp = M_disp_holder.currentDisp();

    if ( ! disp )
    {
        return;
    }

    const rcss::rcg::BallT & ball = disp->show_.ball_;

    for ( int i = 0; i < rcss::rcg::MAX_PLAYER*2; ++i )
    {
        drawAll( painter, disp->show_.player_[i], ball );
    }
}

/*-------------------------------------------------------------------*/
/*

*/

void
ZonePainter::drawAll( QPainter & painter,
        const rcss::rcg::PlayerT & player,
        const rcss::rcg::BallT & ball ) const
{
    const Options & opt = Options::instance();
    const Param param( player,
            ball,
            M_disp_holder.serverParam(),
            M_disp_holder.playerType( player.type_ ) );

    if ( opt.selectedPlayer( param.player_.side(), param.player_.unum_ ) ) {
        drawZone(param.player_.unum_-1, painter);
    } else if ( opt.selectedPlayer( param.player_.side(), 0 ) ) {
        for ( int i = 0; i < 12; i++ ) {
            drawZone(i, painter);
        }
    }
    

    //painter.setBrush( Qt::green );
    //painter.drawRect(x1,y1,x2-x1,y2-y1); 
    //painter.eraseRect(x1,y1,x2-x1,y2-y1); 
    //    drawDir( painter, param );
    //
    //    if ( player.hasNeck()
    //         && player.hasView()
    //         && opt.showViewArea() )
    //    {
    //        drawViewArea( painter, param );
    //    }
    //    drawText( painter, param );
}

const char* colors[] = {
    "darkred",
    "firebrick",
    "indianred",
    "red",
    "coral",
    "gold",
    "yellow",
    "deepskyblue",
    "cornflowerblue",
    "royalblue",
    "midnightblue",
};

void ZonePainter::drawZone( int zone, QPainter & painter ) const {
        int x = Options::instance().screenX(Options::instance().zones()[zone].x1);
        int y = Options::instance().screenY(Options::instance().zones()[zone].y1);
        int width = Options::instance().screenX(Options::instance().zones()[zone].x2) - Options::instance().screenX(Options::instance().zones()[zone].x1);
        int height = Options::instance().screenY(Options::instance().zones()[zone].y2) - Options::instance().screenY(Options::instance().zones()[zone].y1);
        
        QPainterPath path;
        path.addRect( x, y, width, height );
        QPen pen = QPen(colors[zone]);
        pen.setWidth(3);
        //pen.setStyle( Qt::DashLine );
        painter.strokePath( path, pen );
}
