// -*-c++-*-

/*!
  \file player_painer.h
  \brief player painter class Header File.
*/

/*
 *Copyright:

 Copyright (C) The RoboCup Soccer Server Maintenance Group.
 Hidehisa AKIYAMA

 This code is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This code is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this code; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

 *EndCopyright:
 */

/////////////////////////////////////////////////////////////////////

#ifndef RCSSMONITOR_BOX_PAINTER_H
#define RCSSMONITOR_BOX_PAINTER_H

#include <QPen>
#include <QBrush>
#include <QFont>

#include "painter_interface.h"

#include <rcsslogplayer/types.h>

class QPainter;
class QPixmap;

class DispHolder;

class ZonePainter
    : public PainterInterface {
private:

    struct Param {
        int x_; //!< screen X coordinates
        int y_; //!< screen Y coordinates
        int body_radius_; //!< pixel body radius
        int kick_radius_; //!< pixel kick area radius
        int draw_radius_; //!< pixel main draw radius.
        //bool have_full_effort_; //!< flag to check effort value
        const rcss::rcg::PlayerT & player_;
        const rcss::rcg::BallT & ball_;
        const rcss::rcg::PlayerTypeT & player_type_;

        Param( const rcss::rcg::PlayerT & player,
               const rcss::rcg::BallT & ball,
               const rcss::rcg::ServerParamT & sparam,
               const rcss::rcg::PlayerTypeT & ptype );
    private:
        //! not used
        Param();
    };
    struct Zone {
        double x1,x2;
        double y1,y2;
        Zone(double x1, double x2, double y1, double y2);
        Zone(){}
    };


    const DispHolder & M_disp_holder;

    // not used
    ZonePainter();
    ZonePainter( const ZonePainter & );
    const ZonePainter operator=( const ZonePainter & );
public:

    explicit
    ZonePainter( const DispHolder & disp_holder );

    ~ZonePainter();

    void draw( QPainter & dc );

private:

    void drawAll( QPainter & painter,
                  const rcss::rcg::PlayerT & player,
                  const rcss::rcg::BallT & ball ) const;
    void drawText( QPainter & painter,
                   const ZonePainter::Param & param ) const;
    void drawZone( int zone, QPainter & painter ) const;
    QPen pen;
    Zone zones[11];
};

#endif
